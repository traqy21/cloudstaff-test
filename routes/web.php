<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
//web
Route::get('/', "ProjectController@index");
Route::post('import', "ProjectController@uploadFile");

//api
Route::get('/projects', 'ProjectController@getList');
Route::get('/project/{projectId}', 'ProjectController@getProject');
