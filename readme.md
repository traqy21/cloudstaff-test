## Steps to run this project:
- composer install
- composer update
- php artisan database:create
- php artisan migrate
- php artisan serve

## sample xml file
- located in <source-project>/app/storage/test/test.xml

## Endpoints
- http://<host>/
- http://<host>/projects
- http://<host>/project/1

#### Cloudstaff Test by:
- Aries Jay C Traquena
