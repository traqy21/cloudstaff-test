<?php

namespace App\Services;

use App\Repositories\ProjectRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProjectService {

    protected $request;
    protected $repository;

    public function __construct(ProjectRepository $repository) {
        $this->repository = $repository;
    }

    public function setRequest(Request $request) {
        $this->request = $request;
    }

    public function create() {
        return $this->repository->create($this->request->all());
    }

    public function insert($data) {
        return $this->repository->insert($data);
    }

    public function import() {

        try {
            $xmlfile = file_get_contents($this->request->file);
            $encodedData = json_encode(simplexml_load_string($xmlfile));
            $decodedData = json_decode($encodedData);
            $data = [];
            if (isset($decodedData->project) && $decodedData->project !== null) {
                foreach ($decodedData->project as $item) {
                    $data[] = [
                        "name" => $item->name,
                        "description" => $item->description
                    ];
                }
                $this->insert($data);

                return (object) [
                            "message" => "Import successful."
                ];
            }
        } catch (\Exception $e) {
            Log::Debug('Exception', [$e->getMessage()]);
        }

        return (object) [
                    "message" => "Import not successful."
        ];
    }

    public function getList() {
        return (object) [
                    "list" => $this->repository->getAll()
        ];
    }

    public function getProjectById($id) {
        return (object) [
                    "project" => $this->repository->findBy('id', $id)
        ];
    }

}
