<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use App\Services\ProjectService;
use Illuminate\Http\Request;

/**
 * Description of ProjectController
 *
 * @author aries
 */
class ProjectController extends Controller {

    protected $service;

    public function __construct(ProjectService $service) {
        $this->service = $service;
    }

    public function index() {
        return view("index");
    }

    public function uploadFile(Request $request) {
        $this->validate($request, [
            'file' => 'required',
        ]);

        $this->service->setRequest($request);
        $import = $this->service->import();

        return view("success", ["message" => $import->message]);
    }

    //api here
    public function getList() {
        return response()->json($this->service->getList());
    }

    public function getProject($projectId) {
        return response()->json($this->service->getProjectById($projectId));
    }

}
