<?php

namespace App\Repositories;

use App\Models\Project;

class ProjectRepository {

    protected $model;

    public function __construct(Project $model) {
        $this->model = $model;
    }

    public function create(array $data) {
        return $this->model->create($data);
    }

    public function insert(array $data) {
        return $this->model->insert($data);
    }

    public function getAll($columns = array('*')) {
        return $this->model->get($columns);
    }

    public function findBy($attribute, $value, $columns = array('*')) {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

}
