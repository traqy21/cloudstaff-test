#!/bin/sh
chmod 777 -R public/
chmod 777 -R resources/
chmod 777 -R database/
chmod 777 -R storage/


php artisan migrate
php artisan db:seed --class=Traqy\\EasyCore\\Seeds\\DatabaseSeeder
php artisan vendor:publish --force
php artisan config:cache
php artisan cache:clear
composer dump-autoload

echo "Do bcrypt('test') then replace the admin password in db."
php artisan tinker