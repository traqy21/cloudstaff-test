#!/bin/sh
eval `ssh-agent`
ssh-add ~/.ssh/id_rsa_mskbella
cd /home/ubuntu/www/retail-license
git pull origin staging

#uncomment if need to rebuild
#docker-compose -f /home/ubuntu/www/retail-license/staging.yml build
docker-compose -f /home/ubuntu/www/retail-license/staging.yml up -d

echo "Processing npm commands.."
docker exec --user www-data -it staging-retail-license-container-app npm install
docker exec --user www-data -it staging-retail-license-container-app npm run dev

echo "Executing composer update..."
docker exec --user www-data -it staging-retail-license-container-app composer update
echo "Deployed successfully."
