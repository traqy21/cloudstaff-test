<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    <form action="import" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        Select xml file to upload:
                        <br/>
                        <input type="file" name="file" required>
                        <input type="submit" value="Import">
                    </form>
                </div>
                <div class="title m-b-md">
                    <h5>Endpoints:</h5>
                    <ul>
                        <li><a href="http://ec2-54-169-224-223.ap-southeast-1.compute.amazonaws.com:8001">http://ec2-54-169-224-223.ap-southeast-1.compute.amazonaws.com:8001</a></li>
                        <li><a href="http://ec2-54-169-224-223.ap-southeast-1.compute.amazonaws.com:8001/projects">http://ec2-54-169-224-223.ap-southeast-1.compute.amazonaws.com:8001/projects</a></li>
                        <li>
                            <a href="#">http://ec2-54-169-224-223.ap-southeast-1.compute.amazonaws.com:8001/project/{<i>project_id</i>}</a>
                            <i>Please provide the project_id parameter</i>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>
